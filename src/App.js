import "./App.css";
import CompA from "./component/CompA";
import CompB from "./component/CompB";

function App() {
  return (
    <div className="App">
      <CompA />
      <CompB />
    </div>
  );
}

export default App;
