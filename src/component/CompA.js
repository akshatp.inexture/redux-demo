import React from "react";
import { increment, decrement } from "../redux/demo/demoAction";
import { useSelector, useDispatch } from "react-redux";
import { connect } from "react-redux";
import { useStore } from "react-redux";

const CompA = ({ demo1, demo2, increment, decrement }) => {
  // const counter = useSelector((state) => state.demo1.counter);
  //   const display = useSelector((state) => state.demo2.text);
  //   const dispatch = useDispatch();

  const store = useStore();
  console.log(store.getState());

  return (
    <div>
      <h1>{demo1.counter}</h1>
      <h1>{demo2.text}</h1>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
};

const mapStateToProps = (state) => ({ demo1: state.demo1, demo2: state.demo2 });

const mapDispatchToProps = (dispatch) => ({
  increment: () => dispatch(increment()),
  decrement: () => dispatch(decrement()),
});
export default connect(mapStateToProps, mapDispatchToProps)(CompA);
