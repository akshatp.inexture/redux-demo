import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { display } from "../redux/demo2/demo2Action";
import { connect } from "react-redux";

const CompB = ({ display }) => {
  const [input, setInput] = useState("");

  // const dispatch = useDispatch();

  // const handleClick = () => {
  //   dispatch(display(input));
  //   setInput("");
  // };

  return (
    <div>
      <input
        type="text"
        onChange={(e) => setInput(e.target.value)}
        value={input}
      />
      <button onClick={() => display(input)}> set text</button>
    </div>
  );
};
const mapDispatchToProps = (dispatch) => ({
  display: (input) => dispatch(display(input)),
});

export default connect(null, mapDispatchToProps)(CompB);
