import { GET_DATA } from "./demo2Types";

const initialState = {
  text: "",
};
const demo2Reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DATA:
      return {
        ...state,
        text: action.payload,
      };
    default:
      return state;
  }
};

export default demo2Reducer;
