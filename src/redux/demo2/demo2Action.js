import { GET_DATA } from "./demo2Types";

export const display = (input) => {
  console.log(input);
  return {
    type: GET_DATA,
    payload: input,
  };
};
