import { combineReducers } from "redux";
import demo2Reducer from "./demo2/demo2Reducer";
import demoReducer from "./demo/demoReducer";

const rootReducer = combineReducers({
  demo1: demoReducer,
  demo2: demo2Reducer,
});

export default rootReducer;
